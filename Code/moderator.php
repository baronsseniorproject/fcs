<?php
session_start();
 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Moderator Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Outline Admin Theme">
    <meta name="author" content="KaijuThemes">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600' rel='stylesheet'>

    <link type="text/css" href="dashAssets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
    <link type="text/css" href="dashAssets/fonts/themify-icons/themify-icons.css" rel="stylesheet">              <!-- Themify Icons -->
    <link type="text/css" href="dashAssets/fonts/weather-icons/css/weather-icons.min.css" rel="stylesheet">      <!-- Weather Icons -->

    <link rel="stylesheet" href="dashAssets/css/styles-alternative.css" id="theme">             <!-- Default CSS: Altenate Style -->
    <link rel="prefetch alternate stylesheet" href="dashAssets/css/styles.css">                 <!-- Prefetched Secondary Style -->

    <link type="text/css" href="dashAssets/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
    <link type="text/css" href="dashAssets/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

    <!--[if lt IE 10]>
        <script type="text/javascript" src="dashAssets/js/media.match.min.js"></script>
        <script type="text/javascript" src="dashAssets/js/respond.min.js"></script>
        <script type="text/javascript" src="dashAssets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->
    
<link type="text/css" href="dashAssets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"> 						<!-- FullCalendar -->
<link type="text/css" href="dashAssets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"> 			<!-- jVectorMap -->
<link type="text/css" href="dashAssets/plugins/switchery/switchery.css" rel="stylesheet">   							<!-- Switchery -->

<link type="text/css" href="dashAssets/plugins/charts-chartistjs/chartist.min.css" rel="stylesheet"> 					<!-- Chartist -->

    </head>

    <body class="animated-content">
        
        <div class="extrabar">
	<div class="container-fluid">
		
	</div>
</div>
<div class="extrabar-underlay"></div>

<header id="topnav" class="navbar navbar-bluegray navbar-fixed-top">

	<div class="logo-area">
		<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
			<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
				<span class="icon-bg"  href="index.php">
					<i class="ti ti-shift-left"></i>
				</span>
			</a>
		</span>
		
		<a class="navbar-brand" href="moderator.php">Dashboard</a>


	</div><!-- logo-area -->



	<div class="yamm navbar-left navbar-collapse collapse in">
		<ul class="nav navbar-nav">
			

	</div>



	<ul class="nav navbar-nav toolbar pull-right">


		<li class="dropdown toolbar-icon-bg">
			<a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="ti ti-user"></i></span></i></a>
			<ul class="dropdown-menu userinfo arrow">
				<li><a href="#/"><i class="ti ti-user"></i><span>Profile</span></a></li>
				<li><a href="#/"><i class="ti ti-settings"></i><span>Settings</span></a></li>
				<li><a href="#/"><i class="ti ti-help-alt"></i><span>Help</span></a></li>
				<li class="divider"></li>
				<li><a href="#/"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
			</ul>
		</li>

	</ul>

</header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-bluegray">
                    <div class="static-sidebar">
                        <div class="sidebar">
	<div class="widget">
        <div class="widget-body">
            <div class="userinfo">
<!-- 
                <div class="avatar">
                    <img src="http://placehold.it/300&text=Placeholder" class="img-responsive img-circle"> 
                </div>
 -->
                <div class="info">
                    <span class="username"><?php echo $_SESSION['userName']; ?></span>
                </div>
            </div>
        </div>
    </div>
	<div class="widget stay-on-collapse" id="widget-sidebar">
        <nav class="widget-body">
	<ul class="acc-menu">
		<li class="nav-separator"><span>Moderator Permissions</span></li>
		<li><a href="moderator.php"><i class="ti ti-home"></i><span>Dashboard</span></a></li>

		<li><a href="commentApproval.php"><i class="ti ti-pencil"></i><span>Comments</span></a>

		</li>
		

		<li><a href="submitFishForm.php"><i class="ti ti-layout-grid3"></i><span>Suggestion</span></a>


		</li>



	</ul>
</nav>
    </div>


    <!-- <div class="widget" id="widget-sparklines">
        <div class="widget-heading">Sparklines</div>
        <div class="widget-body p-md">
            <div class="clearfix pt-n pb-sm">
                <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Visitors</h5></div>
                <div class="pull-right"><h5 class="m-n">1,785</h5></div>
            </div>
            <div class="spark-totalvisitors"></div>
            <div class="clearfix pt-lg pb-sm">
                <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Earnings</h5></div>
                <div class="pull-right"><h5 class="m-n">$7,585</h5></div>
            </div>
            <div class="spark-totalearnings"></div>
        </div>
    </div> -->
</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                
<li class=""><a href="moderator.php">Home</a></li>
<li class="active"><a href="moderator.php">Dashboard</a></li>

                            </ol>
                            <div class="page-heading">
                                <h1>Dashboard</h1>
                                
                            </div>                                

                            <div class="page-heading">
                                <em><h4>Welcome to the Moderator Dash!</h4></em>
                                
                            </div>                                


	<!-- 
<div class="col-md-9">
		<div class="panel panel-info"  data-widget='{"id" : "wiget9", "draggable": "false"}'>
			<div class="panel-heading">
				<h2>Charts</h2>
				<div class="panel-ctrls button-icon-bg" 
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-colorpicker=''
					data-action-refresh-demo='{"type": "circular"}'
					>
				</div>				
			</div>
			<div class="panel-body">
				<div class="demo-chartist mb-md" id="chart1"></div>

				<div class="row">
					<div class="col-md-3">
						<div class="easypiechart-block">
							<div class="pull-left">
								<h5>Visitors</h5>
								<h4>6,400</h4>
							</div>
							<div class="pull-right">
								<div class="easypiechart" id="dashboard-visitor" data-percent="75">
									<span class="percent"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="easypiechart-block">
							<div class="pull-left">
								<h5>Page Views</h5>
								<h4>16,200</h4>
							</div>
							<div class="pull-right">
								<div class="easypiechart" id="dashboard-pageview" data-percent="60">
									<span class="percent"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="easypiechart-block">
							<div class="pull-left">
								<h5>Item Sold</h5>
								<h4>8,200</h4>
							</div>
							<div class="pull-right">
								<div class="easypiechart" id="dashboard-itemsold" data-percent="50">
									<span class="percent"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="easypiechart-block">
							<div class="pull-left">
								<h5>Earnings</h5>
								<h4>$10,500</h4>
							</div>
							<div class="pull-right">
								<div class="easypiechart" id="dashboard-earning" data-percent="25">
									<span class="percent"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="widget-tasks mb-md">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="widget-chart">
					<h2>Task Progress</h2>
					<div class="widget-body">
						<div class="easypiechart mb-md" id="designprogress" data-percent="25">
							<span class="percent-non"></span>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane" id="widget-tasks">
					<h2>Task Progress</h2>
					<div class="widget-body">
						<div class="easypiechart mb-md" id="codingprogress" data-percent="50">
							<span class="percent-non"></span>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane" id="widget-pending">
					<h2>Task Progress</h2>
					<div class="widget-body">
						<div class="easypiechart mb-md" id="docsprogress" data-percent="75">
							<span class="percent-non"></span>
						</div>
					</div>
				</div>				
			</div>


			<div class="widget-tabs tabdrop-disabled">
				<ul class="nav nav-tabs" role="tablist">
					<li class="active">
						<a href="#widget-chart" aria-controls="widget-chart" role="tab" data-toggle="tab">
							<i class="ti ti-pie-chart design"></i>
							<span>Design</span>
						</a>
					</li>
					<li>
						<a href="#widget-tasks" aria-controls="widget-tasks" role="tab" data-toggle="tab">
							<i class="ti ti-check-box coding"></i>
							<span>Coding</span>
						</a>
					</li>
					<li>
						<a href="#widget-pending" aria-controls="widget-pending" role="tab" data-toggle="tab">
							<i class="ti ti-pencil-alt docs"></i>
							<span>Docs</span>
						</a>
					</li>
				</ul>
			</div>
		</div> <!~~ tasks ~~>

		<div class="widget-weather mb-md">
			<h2>Current Weather</h2>

			<div class="clearfix">
				<div class="pull-left">
					<i class="wi wi-day-lightning mt-xl"></i>
				</div>
				<div class="pull-right">
					<span class="weather-temp">32°</span>
					<span class="weather-temp-range">27° - 35°</span>
					<span class="weather-location">Dhaka, Bangladesh</span>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default demo-new-members" data-widget='{"collapse": "true"}'>
			<div class="panel-heading">
				<h2>Radar Graph</h2>
				<div class="panel-ctrls button-icon-bg" 
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-colorpicker=''
					data-action-refresh-demo='{"type": "circular"}'
					>
				</div>	
			</div>
			<div class="panel-body">
				<canvas id="radarChart" width="360" height="360" class="block" style="margin: 0 auto;"></canvas>
				<p class="text-center mb-sm">Demo multivariate data <span>Progress Report</span></p>
				<p class="text-center text-muted small">Radar charts are helpful for small-to-moderate-sized multivariate data sets</p>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default demo-new-members" data-widget='{"collapse": "true"}'>
			<div class="panel-heading">
				<h2>Sales Graph</h2>
				<div class="panel-ctrls button-icon-bg" 
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-colorpicker=''
					data-action-refresh-demo='{"type": "circular"}'
					>
				</div>
			</div>
			<div class="panel-body">

				<div class="demo-chartist-sales mb-xl" id="chart2"></div>

				<div class="container container-xs-height">
					<div class="row row-xs-height">
					    <div class="col-xs-4 col-xs-height">
							<div class="text-left">
								<h4 class="mt-n mb-sm" style="font-weight: 400;">32%</h4>
								<h5 class="text-muted mt-n mb-xl" style="font-weight: 400;">Channel Sales</h5>
								<h4 class="mt-n mb-sm" style="font-weight: 400;">68%</h4>
								<h5 class="text-muted mt-n mb-n" style="font-weight: 400;">Direct Sales</h5>
							</div>
					    </div>

					    <div class="col-xs-4 col-xs-height col-middle">
							<div class="text-center">
								<h1 class="text-success mb-sm" style="font-weight: 400;">$20,405</h1>
								<h5 class="text-muted mt-n" style="font-weight: 400;">Year 2015</h5>
							</div>
					    </div>

					    <div class="col-xs-4 col-xs-height">
							<div class="text-right">
								<h4 class="mt-n mb-sm" style="font-weight: 400;">$1,630</h4>
								<h5 class="text-muted mt-n mb-xl" style="font-weight: 400;">Total Referrals</h5>
								<h4 class="mt-n mb-sm" style="font-weight: 400;">$10,745</h4>
								<h5 class="text-muted mt-n mb-n" style="font-weight: 400;">Total Earnings</h5>
							</div>
					    </div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default demo-new-members" data-widget='{"collapse": "true"}'>
			<div class="panel-heading">
				<h2>New Members</h2>
				<div class="panel-ctrls button-icon-bg" 
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-colorpicker=''
					data-action-refresh-demo='{"type": "circular"}'
					>
				</div>
			</div>
			<div class="panel-editbox" data-widget-controls=""></div>
				<div class="panel-body panel-no-padding">
					<div class="tabel-responsive">
						<table class="table table-hover m-n">
							<tbody>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Joesph Keitt</td>
									<td class="hidden-xs vam"><a href="mailto:joseph01@xyz.com">joseph01@xyz.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Andrew Hall</td>
									<td class="hidden-xs vam"><a href="mailto:andrew01@xyz.com">andrew01@xyz.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Rhett Hisle</td>
									<td class="hidden-xs vam"><a href="mailto:rhett88@abc.com">rhett88@abc.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Collin Bessette</td>
									<td class="hidden-xs vam"><a href="mailto:collb11@xyz.com">collb11@xyz.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Anderson Corrigan</td>
									<td class="hidden-xs vam"><a href="mailto:andy91@abc.com">andy91@abc.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
								<tr>
									<td class="vam">
										<img src="http://placehold.it/300&text=Placeholder" class="img-tableavatar img-circle">
									</td>
									<td class="vam">Archie Chavarria</td>
									<td class="hidden-xs vam"><a href="mailto:arch77@abc.com">arch77@abc.com</a></td>
									<td class="vam td-btn text-right">
										<div class="btn-group">
											<a href="#" class="btn btn-link"><i class="ti ti-pencil-alt"></i></a>
											<a href="#" class="btn btn-link"><i class="ti ti-close"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
				<div class="panel-footer">
					<ul class="pagination m-n pull-right">
						<li class="disabled"><a href="#"><i class="ti ti-angle-left"></i></a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li  class="active"><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="ti ti-angle-right"></i></a></li>
					</ul>

					<a href="#" class="btn btn-info pull-left"><i class="ti ti-pencil"></i> <span class="hidden-xs">Add Account</span></a>
				</div>
			</div>
	</div>


	<div class="col-md-6">
		<div class="panel panel-default" data-widget='{"collapse": "true"}'>
			<div class="panel-heading">
				<h2>Timeline</h2>
				<div class="panel-ctrls button-icon-bg" 
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-colorpicker=''
					data-action-refresh-demo='{"type": "circular"}'
					>
				</div>
			</div>
			<div class="panel-body scroll-pane has-scrollbar" style="height: 428px;">
				<div class="scroll-content" tabindex="0" style="right: -17px;">
					<ul class="mini-timeline">
						<li class="mini-timeline-indigo">
							<div class="timeline-icon"><i class="ti ti-user"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
								</div>
							</div>
							<span class="timeline-time">4 mins ago</span>
						</li>

						<li class="mini-timeline-deeporange">
							<div class="timeline-icon"><i class="ti ti-check"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
								</div>
							</div>
							<span class="timeline-time">6 mins ago</span>
						</li>

						<li class="mini-timeline-info">
							<div class="timeline-icon"><i class="ti ti-brush"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
								</div>
							</div>
							<span class="timeline-time">12 mins ago</span>
						</li>

						<li class="mini-timeline-indigo">
							<div class="timeline-icon"><i class="ti ti-timer"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
								</div>
							</div>
							<span class="timeline-time">6 hours ago</span>
						</li>
						<li class="mini-timeline-lime">
							<div class="timeline-icon"><i class="ti ti-pie-chart"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
								</div>
	 						</div>
							<span class="timeline-time">4 mins ago</span>
						</li>

						<li class="mini-timeline-deeporange">
							<div class="timeline-icon"><i class="ti ti-user"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
								</div>
							</div>
							<span class="timeline-time">6 mins ago</span>
						</li>

						<li class="mini-timeline-info">
							<div class="timeline-icon"><i class="ti ti-camera"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
								</div>
							</div>
							<span class="timeline-time">12 mins ago</span>
						</li>

						<li class="mini-timeline-indigo">
							<div class="timeline-icon"><i class="ti ti-check"></i></div>
							<div class="timeline-body">
								<div class="timeline-content">
									<a href="#/" class="name block">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
								</div>
							</div>
							<span class="timeline-time">6 hours ago</span>
						</li>
						<li class="mini-timeline-default">
							<div class="timeline-body ml-n">
								<div class="timeline-content">
									<button type="button" data-loading-text="Loading..." class="loading-example-btn btn btn-sm btn-default">See more</button>
								</div>
							</div>
						</li>
					</ul>
				</div>
			<div class="scroll-track"><div class="scroll-thumb" style="height: 46px; transform: translate(0px, 0px);"></div></div></div>
 -->
			<!-- <div class="panel-body">
				<ul class="mini-timeline">
					<li class="mini-timeline-lime">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
							</div>
						</div>
						<span class="timeline-time">4 mins ago</span>
					</li>

					<li class="mini-timeline-deeporange">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
							</div>
						</div>
						<span class="timeline-time">6 mins ago</span>
					</li>

					<li class="mini-timeline-info">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
							</div>
						</div>
						<span class="timeline-time">12 mins ago</span>
					</li>

					<li class="mini-timeline-indigo">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
							</div>
						</div>
						<span class="timeline-time">6 hours ago</span>
					</li>
					<li class="mini-timeline-lime">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
							</div>
 						</div>
						<span class="timeline-time">4 mins ago</span>
					</li>

					<li class="mini-timeline-deeporange">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
							</div>
						</div>
						<span class="timeline-time">6 mins ago</span>
					</li>

					<li class="mini-timeline-info">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
							</div>
						</div>
						<span class="timeline-time">12 mins ago</span>
					</li>

					<li class="mini-timeline-indigo">
						<div class="timeline-icon"><i class="ti ti-user"></i></div>
						<div class="timeline-body">
							<div class="timeline-content">
								<a href="#/" class="name block">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
							</div>
						</div>
						<span class="timeline-time">6 hours ago</span>
					</li>
					<li class="mini-timeline-default">
						<div class="timeline-body ml-n">
							<div class="timeline-content">
								<button type="button" data-loading-text="Loading..." class="loading-example-btn btn btn-sm btn-default">See more</button>
							</div>
						</div>
					</li>
				</ul>
			</div> -->
<!-- 
		</div>
	</div>
 -->
</div>

<!-- <div data-widget-group="group1">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-info" data-widget='{"id" : "wiget9", "draggable": "false"}'>
				<div class="panel-heading">
					<h2>Social Stats</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-editbox" data-widget-controls=""></div>
				<div class="panel-body">
					<div id="socialstats" style="height: 272px;" class="mt-sm mb-sm"></div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-bluegray" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Earnings Stats</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body">
					<div id="earnings" style="height: 272px;" class="mt-sm mb-sm"></div>
				</div>
			</div>
		</div>

	</div>


	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-gray" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                	<h2>Recent Activities</h2>
	                <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body scroll-pane" style="height: 320px;">
					<div class="scroll-content">
						<ul class="mini-timeline">
							<li class="mini-timeline-lime">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
										<span class="time">4 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-deeporange">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
										<span class="time">6 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-info">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
										<span class="time">12 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-indigo">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
										<span class="time">6 hours ago</span>
									</div>
								</div>
							</li>
							<li class="mini-timeline-lime">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Vincent Keller</a> added new task <a href="#/">Admin Dashboard UI</a>
										<span class="time">4 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-deeporange">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Shawna Owen</a> added <a href="#/" class="name">Alonzo Keller</a> and <a href="#/" class="name">Mario Bailey</a> to project <a href="#/">Wordpress eCommerce Template</a>
										<span class="time">6 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-info">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Christian Delgado</a> commented on thread <a href="#/">Frontend Template PSD</a>
										<span class="time">12 mins ago</span>
									</div>
								</div>
							</li>

							<li class="mini-timeline-indigo">
								<div class="timeline-icon"><i class="ti ti-user"></i></div>
								<div class="timeline-body">
									<div class="timeline-content">
										<a href="#/" class="name">Jonathan Smith</a> added <a href="#/" class="name">Frend Pratt</a> and <a href="#/" class="name">Robin Horton</a> to project <a href="#/">Material Design Admin Template</a>
										<span class="time">6 hours ago</span>
									</div>
								</div>
							</li>
							<li class="mini-timeline-default">
								<div class="timeline-body ml-n">
									<div class="timeline-content">
										<button type="button" data-loading-text="Loading..." class="loading-example-btn btn btn-sm btn-default">See more</button>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-white" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                	<h2>New Members</h2>
	                <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body">
					<ul class="widget-avatar">
						<li><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
					</ul>
					<button class="btn btn-block btn-primary">View More</button>
				</div>
			</div>

			<div class="panel panel-white" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                	<h2>Online Now</h2>
	                <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body">
					<ul class="widget-avatar">
						<li data-status="online"><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li data-status="online"><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li data-status="online"><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li data-status="away"><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
						<li data-status="busy"><img src="http://placehold.it/300&text=Placeholder" alt=""/></li>
					</ul>
					<button class="btn btn-block btn-success">Contact List</button>
				</div>
			</div>
		</div>
		<div class="col-md-3">

			<div class="panel panel-midnightblue widget-progress" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2>Progress</h2>
                    <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
                </div>
                <div class="panel-body">
					<div class="easypiechart mb-md" id="progress" data-percent="37">
						<span class="percent-non"></span>
					</div>
                </div>
                <div class="panel-footer">
					<div class="tabular">
						<div class="tabular-row">
							<div class="tabular-cell">
								<span class="status-total">Total</span>
								<span class="status-value">100</span>
							</div>
							<div class="tabular-cell">
								<span class="status-pending">Pending</span>
								<span class="status-value">63</span>
							</div>
						</div>
					</div>
				</div>
            </div>

			<div class="widget-weather">
				<div class="pull-left">
					<span class="weather-location">Toronto, CA</span>
					<span class="weather-desc">Sunny</span>
				</div>
				<div class="pull-right">
					<span class="weather-temp">16<span>ºC</span></span>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Visitor Stats</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="spark-container mb-xl">
								<div class="pull-left">
									<h2 class="title" style="color: #cddc39">Pageviews</h2>
									<h3 class="number">19,600</h3>
								</div>
								<div class="pull-right">
									<h2 class="title" style="color: #ff5722; text-align: right;">Sessions</h2>
									<h3 class="number">1,200</h3>
								</div>

								<div class="spark-pageviews"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div id="newvsreturning" style="height: 144px" class="mt-md mb-md"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="spark-container">
								<h2 class="title">Users</h2>
								<h3 class="number">700</h3>
								<div class="spark-users"></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="spark-container">
								<h2 class="title">Avg. Duration</h2>
								<h3 class="number">00:04:36</h3>
								<div class="spark-avgduration"></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="spark-container">
								<h2 class="title">Page/Session</h2>
								<h3 class="number">4.20</h3>
								<div class="spark-pagesession"></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="spark-container">
								<h2 class="title">Bounce Rate</h2>
								<h3 class="number">52.10%</h3>
								<div class="spark-bouncerate"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-teal" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Browsers</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<tr>
								<td class="vam">Google Chrome</td>
								<td class="text-right">43.7%</td>
								<td class="vam" style="width: 56px;">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 100%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Firefox</td>
								<td class="text-right">20.5%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 50%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Opera</td>
								<td class="text-right">14.6%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 40%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Safari</td>
								<td class="text-right">9.1%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 25%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Internet Explorer</td>
								<td class="text-right">5.3%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 12.5%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Torch</td>
								<td class="text-right">2.9%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 9%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Maxthon</td>
								<td class="text-right">2.3%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 6%"></div>
	                                </div>
	                            </td>
							</tr>
							<tr>
								<td class="vam">Others</td>
								<td class="text-right">1.6%</td>
								<td class="vam">
									<div class="progress m-n">
	                                  <div class="progress-bar progress-bar-teal" style="width: 3%"></div>
	                                </div>
	                            </td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-realtime" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2>Real-Time</h2>
                    <div class="panel-ctrls mr-n">
                    	<div class="mt-md mb-md">
                    		<input type="checkbox" class="js-switch-success switchery-xs" checked />
						</div>
                    </div>
                </div>
                <div class="panel-body">
                	<span class="rightnow">Right Now</span>
					<span class="number">20</span>
					<span class="activeuser">active Users right now</span>
                    <div id="realtime-updates" style="height: 112px" class="centered"></div>
                </div>
            </div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-white" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2>World Map</h2>
                    <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
                </div>
                <div class="panel-body">
					<div id="worldmap" style="height: 272px; width: 100%;" class="mt-sm mb-sm"></div>
                </div>
            </div>
		</div>
		
	</div>

</div> -->

                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    <footer>
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2017 Shoal</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
    </div>
</footer>
                </div>
            </div>
        </div>

    
    <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

<script type="text/javascript" src="dashAssets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script type="text/javascript" src="dashAssets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
<script type="text/javascript" src="dashAssets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->
<script type="text/javascript" src="dashAssets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

<script type="text/javascript" src="dashAssets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="dashAssets/plugins/velocityjs/velocity.ui.min.js"></script>

<script type="text/javascript" src="dashAssets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

<script type="text/javascript" src="dashAssets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="dashAssets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="dashAssets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="dashAssets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="dashAssets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->
<script type="text/javascript" src="dashAssets/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> <!-- Mousewheel support needed for Mapael -->

<script type="text/javascript" src="dashAssets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

<script type="text/javascript" src="dashAssets/js/application.js"></script>
<script type="text/javascript" src="dashAssets/demo/demo.js"></script>
<script type="text/javascript" src="dashAssets/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->
    
    <!-- Load page level scripts-->
    
<script type="text/javascript" src="dashAssets/plugins/charts-chartistjs/chartist.min.js"></script>               <!-- Chartist -->
<script type="text/javascript" src="dashAssets/plugins/charts-chartistjs/chartist-plugin-tooltip.js"></script>    <!-- Chartist -->

<script type="text/javascript" src="dashAssets/plugins/charts-chartjs/Chart.js"></script>  						<!-- Chart.js -->

<script type="text/javascript" src="dashAssets/plugins/easypiechart/jquery.easypiechart.js"></script> 			<!-- EasyPieChart -->
<script type="text/javascript" src="dashAssets/demo/demo-index.js"></script> 									<!-- Initialize scripts for this page-->

    <!-- End loading page level scripts-->

    </body>
</html>