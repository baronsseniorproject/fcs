<?php
session_start();

include('adodb5/adodb.inc.php');
$db = ADONewConnection('mysql'); 


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Outline Admin Theme</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Outline Admin Theme">
    <meta name="author" content="KaijuThemes">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600' rel='stylesheet'>

    <link type="text/css" href="dashAssets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
    <link type="text/css" href="dashAssets/fonts/themify-icons/themify-icons.css" rel="stylesheet">              <!-- Themify Icons -->
    <link type="text/css" href="dashAssets/fonts/weather-icons/css/weather-icons.min.css" rel="stylesheet">      <!-- Weather Icons -->

    <link rel="stylesheet" href="dashAssets/css/styles-alternative.css" id="theme">             <!-- Default CSS: Altenate Style -->
    <link rel="prefetch alternate stylesheet" href="dashAssets/css/styles.css">                 <!-- Prefetched Secondary Style -->

    <link type="text/css" href="dashAssets/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
    <link type="text/css" href="dashAssets/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

    <!--[if lt IE 10]>
        <script type="text/javascript" src="dashAssets/js/media.match.min.js"></script>
        <script type="text/javascript" src="dashAssets/js/respond.min.js"></script>
        <script type="text/javascript" src="dashAssets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->
    

    </head>
    
<?php

$approvedAlert = false;
$ignoredAlert = false;

 // Create a connection handle to the local database 

if($_POST['ignoreButton'])
{
$ignore = $_POST['ignoreCommentId'];
$ignoredAlert = true;	
$conn5 = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') or die ('Cannot connect to db');

$result5 = $conn5->query("delete from comment where id='".$ignore."'");
	
}
if($_POST['approveButton'])
{
$approvedAlert = true;
$approve = $_POST['approveCommentId'];

$conn6 = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') or die ('Cannot connect to db');

$result6 = $conn6->query("update comment set approved=1 where id='".$approve."'");
}



$error= false;
$deleted = false;
$errorMessage = "";

// echo "binomial: " . $binomial . "</br>";
// echo "common: " . $common . "</br>";
// echo "low pH: " . $lowpH . "</br>";
// echo "high pH: " . $highpH. "</br>";
// echo "temper: " . $temper . "</br>";
// echo "size: " . $size . "</br>";
// echo "notes: " . $notes . "</br>";

if(isset($_POST['deleteButton']))
{
$deleteFish=$_POST['deleteFish'];
$conn2 = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') 
or die ('Cannot connect to db');

$result2 = $conn2->query("DELETE FROM fish WHERE id='".$deleteFish."'");
if(isset($result2))
{
$deleted=true;
}
		
}

if(isset($_POST['approveButton']))
{
$approveFish=$_POST['approveFish'];
$conn3 = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') 
or die ('Cannot connect to db');

$result3 = $conn3->query("UPDATE fish SET approved=1 WHERE id='".$approveFish."'");
if(isset($result3))
{
$added=true;
}
		
}	


$conn = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') 
or die ('Cannot connect to db');

$result = $conn->query("select * from fish where approved=0");





?>

    <body class="animated-content">
        
        <div class="extrabar">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-warning">
					<div class="tile-icon"><i class="ti ti-eye"></i></div>
					<div class="tile-heading"><span>Page Views</span></div>
					<div class="tile-body"><span>1,600</span></div>
					<div class="tile-footer"><span class="text-danger">-7.6% <i class="ti ti-arrow-down"></i></span></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-success">
					<div class="tile-icon"><i class="ti ti-thumb-up"></i></div>
					<div class="tile-heading"><span>Likes</span></div>
					<div class="tile-body"><span>345</span></div>
					<div class="tile-footer"><span class="text-success">+15.4% <i class="ti ti-arrow-up"></i></span></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-danger">
					<div class="tile-icon"><i class="ti ti-check-box"></i></div>
					<div class="tile-heading"><span>Bugs Fixed</span></div>
					<div class="tile-body"><span>21</span></div>
					<div class="tile-footer"><span class="text-success">+10.4% <i class="ti ti-arrow-up"></i></span></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-info">
					<div class="tile-icon"><i class="ti ti-user"></i></div>
					<div class="tile-heading"><span>New Members</span></div>
					<div class="tile-body"><span>124</span></div>
					<div class="tile-footer"><span class="text-danger">-25.4% <i class="ti ti-arrow-down"></i></span></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-teal">
					<div class="tile-icon"><i class="ti ti-gift"></i></div>
					<div class="tile-heading"><span>Gifts</span></div>
					<div class="tile-body"><span>16</span></div>
					<div class="tile-footer"><span class="text-danger">-7.6% <i class="ti ti-arrow-down"></i></span></div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="info-tile info-tile-alt tile-indigo">
					<div class="tile-icon"><i class="ti ti-menu-alt"></i></div>
					<div class="tile-heading"><span>Tasks</span></div>
					<div class="tile-body"><span>17</span></div>
					<div class="tile-footer"><span class="text-danger">-26.4% <i class="ti ti-arrow-down"></i></span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="extrabar-underlay"></div>

<header id="topnav" class="navbar navbar-bluegray navbar-fixed-top">

	<div class="logo-area">
		<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
			<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
				<span class="icon-bg"  href="index.php">
					<i class="ti ti-shift-left"></i>
				</span>
			</a>
		</span>
		
		<a class="navbar-brand" href="admin.php">Dashboard</a>


	</div><!-- logo-area -->



	<div class="yamm navbar-left navbar-collapse collapse in">
		<ul class="nav navbar-nav">
			

	</div>



	<ul class="nav navbar-nav toolbar pull-right">


		<li class="dropdown toolbar-icon-bg">
			<a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="ti ti-user"></i></span></i></a>
			<ul class="dropdown-menu userinfo arrow">
				<li><a href="#/"><i class="ti ti-user"></i><span>Profile</span></a></li>
				<li><a href="#/"><i class="ti ti-settings"></i><span>Settings</span></a></li>
				<li><a href="#/"><i class="ti ti-help-alt"></i><span>Help</span></a></li>
				<li class="divider"></li>
				<li><a href="#/"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
			</ul>
		</li>

	</ul>

</header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-bluegray">
                    <div class="static-sidebar">
                        <div class="sidebar">
	<div class="widget">
        <div class="widget-body">
            <div class="userinfo">
<!-- 
                <div class="avatar">
                    <img src="http://placehold.it/300&text=Placeholder" class="img-responsive img-circle"> 
                </div>
 -->
                <div class="info">
                    <span class="username">Glen Maxwell</span>
                </div>
            </div>
        </div>
    </div>
	<div class="widget stay-on-collapse" id="widget-sidebar">
        <nav class="widget-body">
	<ul class="acc-menu">
		<li class="nav-separator"><span>Admin Permissions</span></li>
		<li><a href="admin.php"><i class="ti ti-home"></i><span>Dashboard</span></a></li>

		<li><a href="commentApproval.php"><i class="ti ti-pencil"></i><span>Comments</span></a>

		</li>
		

		<li><a href="adminFishForm.php"><i class="ti ti-layout-grid3"></i><span>Database</span></a>

		</li>



	</ul>
</nav>
    </div>


    <!-- <div class="widget" id="widget-sparklines">
        <div class="widget-heading">Sparklines</div>
        <div class="widget-body p-md">
            <div class="clearfix pt-n pb-sm">
                <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Visitors</h5></div>
                <div class="pull-right"><h5 class="m-n">1,785</h5></div>
            </div>
            <div class="spark-totalvisitors"></div>
            <div class="clearfix pt-lg pb-sm">
                <div class="pull-left"><h5 class="m-n small" style="font-weight: 400;">Total Earnings</h5></div>
                <div class="pull-right"><h5 class="m-n">$7,585</h5></div>
            </div>
            <div class="spark-totalearnings"></div>
        </div>
    </div> -->
</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                
<li><a href="admin.php">Home</a></li>
<li class="active"><a href="adminFishForm.php">Forms</a></li>

                            </ol>
                            <div class="page-heading">
                                <h1>Forms</h1>
 
                            </div>
                            <div class="container-fluid">
                                
<div data-widget-group="group1">
	<div class="row">
		<div class="col-sm-12">
		
<?php
		

	
		if($error==true)
		{
			echo '<div class="alert alert-inverse">';
			echo '<h5><strong>Missing Field:</strong></h5> </br>';
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		}
		
		if($deleted==true)
		{
			echo '<div class="alert alert-inverse">';
			echo '<h5><strong>Record Deleted</strong></h5> </br>';
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		}
		if($added==true)
		{
			echo '<div class="alert alert-inverse">';
			echo '<h5><strong>Record Added</strong></h5> </br>';
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		}
				?>
			</div>
			<div class="panel panel-midnightblue" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Admin Search</h2>
					<!-- <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body, .panel-footer"}'></div> -->
			        <div>
						<ul class="nav nav-tabs">
			              <li><a href="adminFishForm.php">Add Form</a></li>
			              <li><a href="adminSearchForm.php">Search and Modify</a></li>
			              <li><a href="adminAddForm.php">View New Forms</a></li>

			            </ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						
<?php

// $binomial = $_POST['binomial'];
// $common = $_POST['common'];
// $lowpH = $_POST['lowpH'];
// $highpH = $_POST['highpH'];
// $temper = $_POST['radioTemper'];
// $size = $_POST['radioSize'];
// $notes = $_POST['notes'];
// $error= false;
// $errorMessage = "";
// 
// echo "binomial: " . $binomial . "</br>";
// echo "common: " . $common . "</br>";
// echo "low pH: " . $lowpH . "</br>";
// echo "high pH: " . $highpH. "</br>";
// echo "temper: " . $temper . "</br>";
// echo "size: " . $size . "</br>";
// echo "notes: " . $notes . "</br>";
// 
// if(empty($binomial))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing Binomial </br>";
// }
// if(empty($common))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing Common Name </br>";
// 
// }
// if(empty($lowpH))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing Low pH </br>";
// 
// }
// if(empty($highpH))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing High pH</br>";
// 
// }
// if(empty($temper))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing Temper </br>";
// 
// }
// if(empty($size))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing size</br>";
// 
// }
// if(empty($notes))
// {
// $error=true;
// $errorMessage = $errorMessage . "missing notes</br>";
// 
// }




?>				

					
	

					</div>

				</div>
	</div>
			

		</div>
	</div>



</div>
<?
if(isset($result))
{
?>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Search Result</h2>
					<div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
					<div class="options">

					</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped m-n">
						<thead>
							<tr>
								<th>#</th>
								<th>Binomial</th>
								<th>Common</th>
								<th>Temperature</th>
								<th>pH</th>
								<th>Temper</th>
								<th>size</th>
								<th>Image</th>
								<th> </th>
								<th> </th>t
							</tr>
						</thead>
						<tbody>
						
<?php

while ($row = $result->fetch_assoc()) {

                  $id = $row['id'];
                  $binomial = $row['binomial'];
                  $common = $row['common']; 
                  $lowTemp = $row['lowTemp'];
                  $highTemp=$row['highTemp'];
                  $lowpH = $row['lowpH'];
                  $highpH = $row['highpH'];
                  $temper= $row['temper'];
                  $size = $row['size'];
                  $notes = $row['notes'];
                  $imgPath =$row['imgPath'];
                  
                  

echo '<tr>';
echo '<th>'.$id.'</th>';
echo '<th>'.$binomial.'</th>';
echo '<th>'.$common.'</th>';
echo '<th>'.$lowTemp.' - '. $highTemp.'</th>';
echo '<th>'.$lowpH.' - '. $highpH.'</th>';
echo '<th>'.$temper.'</th>';
echo '<th>'.$size.'</th>';
echo '<th><a href="'.$imgPath.'">Image</th>';

echo '<th>
<form action="'.$_SERVER['PHP_SELF'].'" method="post" name="deleteForm">
<input type="submit" name="deleteButton" value="Delete">
<input type="hidden" name="deleteFish" value="'.$id.'"
</th>
</form>';

echo '<th>
<form action="'.$_SERVER['PHP_SELF'].'" method="post" name="approveForm">
<input type="submit" name="approveButton" value="Approve">
<input type="hidden" name="approveFish" value="'.$id.'"
</th>
</form>';


echo '</tr>';              
}

?>							
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<?php
	}
	?>


                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    <footer>
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2015 KaijuThemes</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
    </div>
</footer>
                </div>
            </div>
        </div>

    
   
    <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

<script type="text/javascript" src="dashAssets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script type="text/javascript" src="dashAssets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
<script type="text/javascript" src="dashAssets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->
<script type="text/javascript" src="dashAssets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

<script type="text/javascript" src="dashAssets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="dashAssets/plugins/velocityjs/velocity.ui.min.js"></script>

<script type="text/javascript" src="dashAssets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

<script type="text/javascript" src="dashAssets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="dashAssets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="dashAssets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="dashAssets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="dashAssets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->
<script type="text/javascript" src="dashAssets/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> <!-- Mousewheel support needed for Mapael -->

<script type="text/javascript" src="dashAssets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

<script type="text/javascript" src="dashAssets/js/application.js"></script>
<script type="text/javascript" src="dashAssets/demo/demo.js"></script>
<script type="text/javascript" src="dashAssets/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->
    
    <!-- Load page level scripts-->
    

    <!-- End loading page level scripts-->

    </body>
</html>