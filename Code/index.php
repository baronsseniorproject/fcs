<?php

session_start();

include('adodb5/adodb.inc.php');

$db = ADONewConnection('mysql'); // Create a connection handle to the local database 

// Open a connection -- pass in the localhost, username, 
//password and database name 
// 

$conn = new mysqli('localhost', 'bheinric_admin', 'WishyFishy1', 'bheinric_fish') 
or die ('Cannot connect to db');


$result = $conn->query("SELECT * FROM fish where approved=1");

$row = $result->fetch_assoc();

//   while ($row = $result->fetch_assoc()) {
//     			
//                 $name = $row['common'];
//                 }

?>

<!DOCTYPE html>
<html>
	<head>

		<title>Shoal || Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta charset="UTF-8" />

		<!-- Bootstrap -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen" />

		<!-- Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet" type="text/css" />
		<link href="assets/css/entypo.css" rel="stylesheet" type="text/css" />

		<!-- Template CSS -->
		<link href="assets/css/686tees.css" rel="stylesheet" type="text/css" />
		
		<style>
		

		</style>

	</head>
<body>

<div class="container">

	<!-- Site Top -->

	<div class="row">

		<div class="col-md-12">
<?php

	if(empty($_SESSION['userName']))
	{
?>
			<ul class="top-nav">
				<li><a href="login.php">Login</a></li>
				<li><a href="register.html">Registration</a></li>
				<li><a href="forgot.html">Lost your password?</a></li>
			</ul>
<?php
	}
	else
	{
	
	
	echo '<ul class="top-nav">';
	
	if($_SESSION['role']==0)
	{
		echo '<li><a href="admin.php">Hello, '.$_SESSION['userName'].'!</a></li>';	

	}
	else
	{
		echo '<li>Hello, '.$_SESSION['userName'].'!</li>';	
	}
	echo '</ul>';
	}
?>
		</div>

	</div>

	<!-- Header -->

	<div class="row">

		<div class="col-md-4 col-sm-4">

			<a class="logo" href="index.html">
				<span class="entypo heart"></span> Shoal
			</a>

		</div>

		<div class="col-md-4 col-sm-5">


		</div>

		<div class="col-md-4 col-sm-3">

			<div class="row">

				<div class="col-md-6 col-md-offset-6 mini-basket">

					<p class="mini-basket-title"><a href="basket.php">My Tank</a></p>

				</div>

			</div>

		</div>

	</div>

	<!-- Menu -->

	<div class="row">

		<div class="col-md-12">

			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">

			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			    </div>

			    <div class="collapse navbar-collapse" id="navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="index.html">New</a></li>
			        <li><a href="products-men.html">Mens</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Womens <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="products-women.html">Tops</a></li>
			            <li><a href="products-women.html">Accessories</a></li>
			            <li><a href="products-women.html">Shoes</a></li>
			            <li><a href="products-women.html">Dresses</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="index-alt.html">Alternate Homepage</a></li>
			            <li><a href="pricing.html">Price Comparison</a></li>
			            <li><a href="faq.html">FAQ</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="blog-listing.html">Blog Listing</a></li>
			            <li><a href="blog-detail.html">Blog Detail</a></li>
			          </ul>
			        </li>
			      </ul>
			      <form class="navbar-form navbar-right clearfix" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default"><span class="entypo search"></span></button>
			      </form>
			    </div>
			  </div>
			</nav>

		</div>

	</div>

	<!-- Body -->

	<div class="row">

		<div class="col-md-12">

			<h1 class="catalogue-title">Fish</h1>

		</div>

	</div>

	<!-- Product Listing -->

	<div class="row">
	
	<?php
    $count = 1;
    
    while ($row = $result->fetch_assoc()) 
    {
    
    if($count%3==0)
    {
    	echo '<div class="row">';
    }
    			
                $name = $row['common']; 
                $id = $row['id'];
                $imgPath = $row['imgPath'];

				echo '<div class="col-md-4 col-sm-4 product-listing">';
				echo '<p class="title"><a href="product.php">'.$name.'</a></p>';
				echo '<a href="product.php?id='.$id.'"><img class="image" src="'.$imgPath.'" alt="'.$name.'" /></a>';
				echo '</p>';
				echo '</div>';
				
	if($count%3==0)
	{
		echo '</div>';
		$count=0;
	}
				$count = $count+1;
				
                 
}
echo '</div>';
?>
<!-- 
		<div class="col-md-4 col-sm-4 product-listing">

			<p class="title"><a href="product.html">Ibiza Lips</a></p>

			<a href="product.html"><img class="image" src="assets/img/product-1.jpg" alt="Ibiza Lips" /></a>

			<p class="price">
				&pound; 9.99
				<a class="btn btn-addcart btn-primary" href="basket.html"><span class="entypo cart"></span></a><a class="btn btn-view btn-grey" href="product.html"><span class="entypo search"></span></a>
			</p>

		</div>

		<div class="col-md-4 col-sm-4 product-listing">

			<p class="title"><a href="product.html">Ibiza Banana</a></p>

			<a href="product.html"><img class="image" src="assets/img/product-2.jpg" alt="Ibiza Banana" /></a>

			<p class="price">
				&pound; 9.99
				<a class="btn btn-addcart btn-primary" href="basket.html"><span class="entypo cart"></span></a><a class="btn btn-view btn-grey" href="product.html"><span class="entypo search"></span></a>
			</p>

		</div>

		<div class="col-md-4 col-sm-4 product-listing">

			<p class="title"><a href="product.html">I Was There</a></p>

			<a href="product.html"><img class="image" src="assets/img/product-3.jpg" alt="I Was There" /></a>

			<p class="price">
				&pound; 9.99
				<a class="btn btn-addcart btn-primary" href="basket.html"><span class="entypo cart"></span></a><a class="btn btn-view btn-grey" href="product.html"><span class="entypo search"></span></a>
			</p>

		</div>

	</div>
 -->



	<div class="row footer">

		<div class="col-md-6">

			<ul class="footer-nav">
				<li><a href=""><img src="assets/flags/gb.png" alt="GBP" /></a> &nbsp; <a href=""><img src="assets/flags/us.png" alt="USD" /></a> &nbsp; <a href=""><img src="assets/flags/europeanunion.png" alt="Euro" /></a></li>
				<li><a href="content.html">Terms &amp; Conditions</a></li>
				<li><a href="content.html">Delivery Information</a></li>
				<li><a href="contact.html">Contact</a></li>
			</ul>

		</div>

		<div class="col-md-6 footer-right">

			<p>
				&copy; 686 Tees
			</p>

		</div>

	</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
